package anosym.geoip.provider.freegeoip;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsNull.nullValue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

import org.apache.http.client.methods.HttpGet;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import anosym.common.Country;
import anosym.geoip.GeoIp;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Oct 5, 2015, 12:45:21 AM
 */
@ExtendWith(MockitoExtension.class)
public class FreeGeoIpProviderTest {

	private static final String GEOIP_DATA = "" + "{\n" + "    \"ip\": \"157.55.39.37\",\n"
			+ "    \"country_code\": \"US\",\n" + "    \"country_name\": \"United States\",\n"
			+ "    \"region_code\": \"WA\",\n" + "    \"region_name\": \"Washington\",\n"
			+ "    \"city\": \"Redmond\",\n" + "    \"zip_code\": \"98052\",\n"
			+ "    \"time_zone\": \"America/Los_Angeles\",\n" + "    \"latitude\": 47.674,\n"
			+ "    \"longitude\": -122.122,\n" + "    \"metro_code\": 819\n" + "}";

	@Mock
	private FreeGeoIpConfigurationService freeGeoIpConfigurationService;

	@Spy
	@InjectMocks
	private FreeGeoIpProvider freeGeoIpProvider;

	@BeforeEach
	public void setUp() {
		when(freeGeoIpConfigurationService.getEndPointUrl()).thenReturn("https://freegeoip.net/json/");
	}

	@Test
	public void testGeoIpData() throws IOException {
		final InputStream inputStream = new ByteArrayInputStream(GEOIP_DATA.getBytes());
		doReturn(inputStream).when(freeGeoIpProvider).doExecuteGet(anyString(), any(HttpGet.class));

		final GeoIp geoIp = freeGeoIpProvider.provide("157.55.39.37");
		assertThat(geoIp, is(not(nullValue())));
		assertThat(geoIp.getIp(), is("157.55.39.37"));
		assertThat(geoIp.getCountry(), is(Country.UNITED_STATES));
	}

}
