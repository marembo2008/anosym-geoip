package anosym.geoip.provider.nekudo;

import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNull.nullValue;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;

import java.io.ByteArrayInputStream;
import java.io.IOException;

import org.apache.http.client.methods.HttpGet;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import anosym.common.Country;
import anosym.geoip.GeoIp;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Oct 4, 2015, 11:25:09 AM
 */
@ExtendWith(MockitoExtension.class)
public class NekudoGeoIpProviderTest {

	private static final String GEOIP_INFO = "" + "{\n" + "  \"city\": \"San Francisco\",\n" + "  \"country\": {\n"
			+ "    \"name\": \"United States\",\n" + "    \"code\": \"US\"\n" + "  },\n" + "  \"location\": {\n"
			+ "    \"latitude\": 37.7697,\n" + "    \"longitude\": -122.3933,\n"
			+ "    \"time_zone\": \"America/Los_Angeles\"\n" + "  },\n" + "  \"ip\": \"198.41.190.243\"\n" + "}";

	@Mock
	private NekudoGeoIpConfigurationService nekudoGeoIpConfigurationService;

	@Spy
	@InjectMocks
	private NekudoGeoIpProvider nekudoGeoIpProvider;

	@Test
	public void testGeoIpLoading() throws IOException {
		final ByteArrayInputStream inputStream = new ByteArrayInputStream(GEOIP_INFO.getBytes());
		final String ip = "198.41.190.243";
		final HttpGet httpGet = mock(HttpGet.class);
		doReturn(httpGet).when(nekudoGeoIpProvider).createHttpGet(ip);
		doReturn(inputStream).when(nekudoGeoIpProvider).doExecuteGet(ip, httpGet);

		final GeoIp geoIp = nekudoGeoIpProvider.provide(ip);
		assertThat(geoIp, is(not(nullValue())));
		assertThat(geoIp.getCity(), is("San Francisco"));
		assertThat(geoIp.getCountry(), is(Country.UNITED_STATES));
		assertThat(geoIp.getIp(), is(ip));
	}

}
