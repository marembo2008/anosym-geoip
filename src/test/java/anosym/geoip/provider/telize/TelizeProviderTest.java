package anosym.geoip.provider.telize;

import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNull.nullValue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

import org.apache.http.client.methods.HttpGet;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import anosym.common.Country;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Aug 2, 2015, 6:15:21 PM
 */
@ExtendWith(MockitoExtension.class)
public class TelizeProviderTest {

	private static final String TEST_GEOIP_INFO = "" + "{\n" + "    \"dma_code\": \"0\",\n"
			+ "    \"ip\": \"92.78.45.243\",\n" + "    \"asn\": \"AS3209\",\n" + "    \"city\": \"Berlin\",\n"
			+ "    \"latitude\": 52.5167,\n" + "    \"country_code\": \"DE\",\n" + "    \"offset\": \"2\",\n"
			+ "    \"country\": \"Germany\",\n" + "    \"region_code\": \"16\",\n" + "    \"isp\": \"Vodafone GmbH\",\n"
			+ "    \"timezone\": \"Europe\\/Berlin\",\n" + "    \"area_code\": \"0\",\n"
			+ "    \"continent_code\": \"EU\",\n" + "    \"longitude\": 13.4,\n" + "    \"region\": \"Berlin\",\n"
			+ "    \"postal_code\": \"10317\",\n" + "    \"country_code3\": \"DEU\"\n" + "}\n" + "\n" + "";

	@Mock
	private TelizeConfigurationService telizeConfigurationServices;

	@Spy
	@InjectMocks
	private TelizeGeoIpProvider telizeProvider;

	@BeforeEach
	public void setUp() {
		when(telizeConfigurationServices.getEndPointUrl()).thenReturn("https://www.telize.com/geoip/");
	}

	@Test
	public void testGeoipInfo() throws IOException {
		final InputStream inputStream = new ByteArrayInputStream(TEST_GEOIP_INFO.getBytes());
		doReturn(inputStream).when(telizeProvider).doExecuteGet(anyString(), any(HttpGet.class));

		final String ip = "92.78.45.243";
		final TelizeGeoIpInfo geoipInfo = telizeProvider.executeGet(ip, TelizeGeoIpInfo.class);
		assertThat(geoipInfo, is(not(nullValue())));
		assertThat(geoipInfo.getIp(), is(ip));
		assertThat(geoipInfo.getCountry(), is(Country.GERMANY));
	}

}
