package anosym.geoip.provider.maxmind;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import anosym.common.Country;
import anosym.geoip.GeoIp;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Oct 9, 2015, 6:28:08 PM
 */
@ExtendWith(MockitoExtension.class)
public class MaxmindGeoIpProviderTest {

	@InjectMocks
	private MaxmindGeoIpProvider maxmindGeoIpProvider;

	@BeforeEach
	public void setUp() {
		maxmindGeoIpProvider.initDatabaseReader();
	}

	@Test
	public void testUsIp() {
		final GeoIp geoIp = maxmindGeoIpProvider.provide("4.205.5.3");
		assertThat(geoIp, is(not(nullValue())));
		assertThat(geoIp.getCountry(), is(Country.UNITED_STATES));
	}

	@Test
	public void testKenyaIp() {
		final GeoIp geoIp = maxmindGeoIpProvider.provide("41.57.111.56");
		assertThat(geoIp, is(not(nullValue())));
		assertThat(geoIp.getCountry(), is(Country.KENYA));
	}

	@Test
	public void testZimbwabweIp() {
		final GeoIp geoIp = maxmindGeoIpProvider.provide("41.85.223.2");
		assertThat(geoIp, is(not(nullValue())));
		assertThat(geoIp.getCountry(), is(Country.ZIMBABWE));
	}

}
