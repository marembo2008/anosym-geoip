package anosym.geoip.provider.telize;

import java.math.BigDecimal;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import anosym.common.Country;
import anosym.geoip.provider.jsonutil.CountryCodeDserializer;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Aug 2, 2015, 5:21:08 PM
 */
@Getter
@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor(access = AccessLevel.PROTECTED)
class TelizeGeoIpInfo {

	@Nullable
	private BigDecimal longitude;

	@Nullable
	private BigDecimal latitude;

	@Nullable
	private String asn;

	@Nullable
	private String offset;

	@Nonnull
	private String ip;

	@Nullable
	@JsonProperty("area_code")
	private String areaCode;

	@Nullable
	@JsonProperty("continent_code")
	private String continentCode;

	@Nullable
	@JsonProperty("dma_code")
	private String dmaCode;

	@Nullable
	private String city;

	@Nullable
	private String timezone;

	@Nullable
	private String region;

	@Nonnull
	@JsonProperty("country_code")
	@JsonDeserialize(using = CountryCodeDserializer.class)
	private Country country;

	@Nonnull
	@JsonProperty("country")
	private String countryName;

	@Nullable
	private String isp;

	@Nullable
	@JsonProperty("region_code")
	private String regionCode;

}
