package anosym.geoip.provider.telize;

import javax.annotation.Nonnull;

import anosym.geoip.provider.ProviderConfigurationService;
import khameleon.core.annotations.Config;
import khameleon.core.annotations.ConfigRoot;
import khameleon.core.annotations.Default;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Aug 2, 2015, 5:33:45 PM
 */
@Config
@ConfigRoot
public interface TelizeConfigurationService extends ProviderConfigurationService {

	@Nonnull
	@Override
	@Default("https://www.telize.com/geoip/")
	public String getEndPointUrl();

}
