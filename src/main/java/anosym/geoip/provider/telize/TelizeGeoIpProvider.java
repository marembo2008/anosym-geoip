package anosym.geoip.provider.telize;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Strings.nullToEmpty;

import javax.annotation.Nonnull;

import anosym.geoip.GeoIp;
import anosym.geoip.provider.GeoIpProvider;
import anosym.geoip.provider.ProviderConfigurationService;
import anosym.geoip.provider.ProviderScope;
import anosym.geoip.provider.RestServiceProvider;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Aug 2, 2015, 5:35:00 PM
 */
@ApplicationScoped
@ProviderScope(id = "http://www.telize.com")
public class TelizeGeoIpProvider extends RestServiceProvider implements GeoIpProvider {

	@Inject
	private TelizeConfigurationService telizeConfigurationService;

	@Nonnull
	@Override
	protected ProviderConfigurationService providerConfigurationService() {
		return telizeConfigurationService;
	}

	@Override
	public GeoIp provide(@Nonnull final String ip) {
		checkNotNull(ip, "The ip must not be null");

		final TelizeGeoIpInfo geoIpInfo = executeGet(ip, TelizeGeoIpInfo.class);
		if (geoIpInfo == null) {
			return null;
		}

		return GeoIp.builder().city(nullToEmpty(geoIpInfo.getCity())).country(geoIpInfo.getCountry())
				.countryName(geoIpInfo.getCountryName()).ip(geoIpInfo.getIp()).build();
	}

}
