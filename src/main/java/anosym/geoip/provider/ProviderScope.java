package anosym.geoip.provider;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import jakarta.enterprise.context.Dependent;
import jakarta.enterprise.inject.Stereotype;

/**
 *
 * @author marembo
 */
@Stereotype
@Retention(RUNTIME)
@Target({TYPE})
@Documented
@Inherited
@Dependent
public @interface ProviderScope {

  /**
   * The id of the provider, must be unique.
   *
   * @return
   */
  String id();

  /**
   * smaller values indicate higher priorities.
   *
   * @return
   */
  int priority() default 0;

}
