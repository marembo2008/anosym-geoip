package anosym.geoip.provider;

import java.io.Serializable;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import anosym.geoip.GeoIp;

/**
 *
 * @author marembo
 */
public interface GeoIpProvider extends Serializable {

	@Nullable
	GeoIp provide(@Nonnull final String ip);

	@Nullable
	default String getId() {
		final ProviderScope providerScope = getClass().getAnnotation(ProviderScope.class);
		if (providerScope != null) {
			return providerScope.id();
		}

		return null;
	}
}
