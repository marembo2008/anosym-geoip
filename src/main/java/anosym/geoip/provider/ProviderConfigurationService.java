package anosym.geoip.provider;

import javax.annotation.Nonnull;

import khameleon.core.annotations.Config;
import khameleon.core.annotations.ConfigRoot;
import khameleon.core.annotations.Default;
import khameleon.core.annotations.Info;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Oct 4, 2015, 11:02:03 AM
 */
@Config
@ConfigRoot
public interface ProviderConfigurationService {

	@Nonnull
	@Default("http://geoip.nekudo.com/api")
	@Info("The api endpoint for the GeoIp provider")
	String getEndPointUrl();

	@Default("100")
	@Info("Connection timeout in milliseconds")
	int getConnectionTimeout();

	@Default("100")
	@Info("Read timeout in milliseconds")
	int getReadTimeout();
}
