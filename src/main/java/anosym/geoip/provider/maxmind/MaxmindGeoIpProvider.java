package anosym.geoip.provider.maxmind;

import java.io.IOException;
import java.io.InputStream;
import java.net.InetAddress;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.Throwables;
import com.maxmind.geoip2.DatabaseReader;
import com.maxmind.geoip2.exception.GeoIp2Exception;
import com.maxmind.geoip2.model.CountryResponse;

import anosym.common.Country;
import anosym.geoip.GeoIp;
import anosym.geoip.provider.GeoIpProvider;
import anosym.geoip.provider.ProviderScope;
import jakarta.annotation.PostConstruct;
import jakarta.enterprise.context.ApplicationScoped;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Oct 9, 2015, 6:08:09 PM
 */
@ApplicationScoped
@ProviderScope(id = "dev.maximind.com", priority = -10)
public class MaxmindGeoIpProvider implements GeoIpProvider {

	private static final Logger LOG = Logger.getLogger(MaxmindGeoIpProvider.class.getName());

	private DatabaseReader databaseReader;

	@PostConstruct
	@VisibleForTesting
	void initDatabaseReader() {
		try {
			final InputStream dbInputStream = getClass().getResourceAsStream("/maximind/GeoLite2-Country.mmdb");
			if (dbInputStream == null) {
				LOG.warning("Unable to read Maximind GeoIp2 Lite db from classpath");
				return;
			}

			databaseReader = new DatabaseReader.Builder(dbInputStream).build();
		} catch (final IOException ex) {
			LOG.log(Level.SEVERE, "Failure Starting up Maximind GeoIp Database Reader", ex);
			throw new RuntimeException(ex);
		}
	}

	@Nullable
	@Override
	public GeoIp provide(@Nonnull final String ip) {
		if (databaseReader == null) {
			return null;
		}

		try {
			final InetAddress inetAddress = InetAddress.getByName(ip);
			final CountryResponse countryResponse = databaseReader.country(inetAddress);
			if (countryResponse == null) {
				return null;
			}

			final com.maxmind.geoip2.record.Country country = countryResponse.getCountry();
			if (country == null) {
				return null;
			}

			return GeoIp.builder().country(Country.fromIsoCode(country.getIsoCode())).countryName(country.getName())
					.ip(ip).build();
		} catch (final IOException | GeoIp2Exception ex) {
			Throwables.throwIfUnchecked(ex);
			throw new RuntimeException(ex);
		}
	}

}
