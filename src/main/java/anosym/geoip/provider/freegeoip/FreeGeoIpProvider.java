package anosym.geoip.provider.freegeoip;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import anosym.geoip.GeoIp;
import anosym.geoip.provider.GeoIpProvider;
import anosym.geoip.provider.ProviderConfigurationService;
import anosym.geoip.provider.ProviderScope;
import anosym.geoip.provider.RestServiceProvider;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;

/**
 *
 * @author marembo
 */
@ApplicationScoped
@ProviderScope(id = "http://freegeoip.net")
public class FreeGeoIpProvider extends RestServiceProvider implements GeoIpProvider {

	@Inject
	private FreeGeoIpConfigurationService freeGeoIpConfigurationService;

	@Nonnull
	@Override
	protected ProviderConfigurationService providerConfigurationService() {
		return freeGeoIpConfigurationService;
	}

	@Override
	@Nullable
	public GeoIp provide(@Nonnull final String ip) {
		return executeGet(ip, GeoIp.class);
	}

}
