package anosym.geoip.provider.freegeoip;

import javax.annotation.Nonnull;

import anosym.geoip.provider.ProviderConfigurationService;
import khameleon.core.annotations.Config;
import khameleon.core.annotations.ConfigRoot;
import khameleon.core.annotations.Default;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Oct 5, 2015, 12:42:43 AM
 */
@Config
@ConfigRoot
public interface FreeGeoIpConfigurationService extends ProviderConfigurationService {

    @Nonnull
    @Override
    @Default("https://freegeoip.net/json/")
    public String getEndPointUrl();

}
