package anosym.geoip.provider.jsonutil;

import java.io.IOException;

import javax.annotation.Nonnull;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;

import anosym.common.Country;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Oct 4, 2015, 11:21:14 AM
 */
public class CountryCodeDserializer extends JsonDeserializer<Country> {

	@Override
	public Country deserialize(@Nonnull final JsonParser p, @Nonnull final DeserializationContext ctxt)
			throws IOException, JsonProcessingException {
		final ObjectCodec objectCodec = p.getCodec();
		final JsonNode countryCodeNode = objectCodec.readTree(p);
		if (countryCodeNode == null) {
			return null;
		}

		final String countryCode = countryCodeNode.asText();
		return Country.fromIsoCode(countryCode);
	}
}
