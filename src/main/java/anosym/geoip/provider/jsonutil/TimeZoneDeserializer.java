package anosym.geoip.provider.jsonutil;

import java.io.IOException;
import java.util.TimeZone;

import javax.annotation.Nonnull;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Oct 4, 2015, 10:54:30 AM
 */
public class TimeZoneDeserializer extends JsonDeserializer<TimeZone> {

	@Override
	public TimeZone deserialize(@Nonnull final JsonParser p, @Nonnull final DeserializationContext ctxt)
			throws IOException, JsonProcessingException {
		final ObjectCodec objectCodec = p.getCodec();
		final JsonNode timeZoneNode = objectCodec.readTree(p);
		if (timeZoneNode == null) {
			return null;
		}

		final String timeZoneId = timeZoneNode.asText();
		return TimeZone.getTimeZone(timeZoneId);
	}

}
