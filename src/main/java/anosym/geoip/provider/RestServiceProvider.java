package anosym.geoip.provider;

import static java.lang.String.format;

import java.io.IOException;
import java.io.InputStream;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.CharMatcher;
import com.google.common.base.Throwables;

import anosym.profiler.Profile;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Oct 5, 2015, 12:29:41 AM
 */
public abstract class RestServiceProvider {

	protected static final CloseableHttpClient HTTP_CLIENT = HttpClients.createDefault();

	private static final String CONTENT_TYPE = "Content-Type";

	private static final String CONTENT_TYPE_JSON = "application/json";

	private static final CharMatcher FORWARD_SLASH = CharMatcher.is('/');

	protected static final ObjectMapper OBJECT_MAPPER = new ObjectMapper()
			.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

	@Nonnull
	protected abstract ProviderConfigurationService providerConfigurationService();

	@Profile
	@Nullable
	@VisibleForTesting
	public <T> T executeGet(@Nonnull final String ip, @Nonnull final Class<T> type) {
		final HttpGet httpGet = createHttpGet(ip);
		try {
			final InputStream inputStream = doExecuteGet(ip, httpGet);
			if (inputStream == null) {
				return null;
			}

			return OBJECT_MAPPER.readValue(inputStream, type);
		} catch (final IOException ioe) {
			throw Throwables.propagate(ioe);
		} finally {
			httpGet.releaseConnection();
		}
	}

	@Nullable
	@VisibleForTesting
	public InputStream doExecuteGet(@Nonnull final String ip, @Nonnull final HttpGet httpGet) throws IOException {
		try (final CloseableHttpResponse httpResponse = HTTP_CLIENT.execute(httpGet)) {
			final StatusLine statusLine = httpResponse.getStatusLine();
			final int status = statusLine.getStatusCode();
			if (status == HttpStatus.SC_NO_CONTENT) {
				return null;
			}

			if (status != HttpStatus.SC_OK) {
				throw new IOException(format("{%s, %s}", status, statusLine.getReasonPhrase()));
			}

			return httpResponse.getEntity().getContent();
		}
	}

	@Nonnull
	@VisibleForTesting
	public HttpGet createHttpGet(@Nonnull final String ip) {
		final ProviderConfigurationService providerConfigurationService = providerConfigurationService();
		final String endpointUrl = providerConfigurationService.getEndPointUrl();
		final String endpointUrlWithParameter = format("%s/%s", FORWARD_SLASH.trimTrailingFrom(endpointUrl), ip);

		final HttpGet httpGet = new HttpGet(endpointUrlWithParameter);
		final RequestConfig requestConfig = RequestConfig.custom()
				.setConnectTimeout(providerConfigurationService.getConnectionTimeout())
				.setConnectionRequestTimeout(providerConfigurationService.getConnectionTimeout())
				.setSocketTimeout(providerConfigurationService.getReadTimeout()).build();
		httpGet.setConfig(requestConfig);

		return setHeaders(httpGet);
	}

	@Nonnull
	private <T extends HttpUriRequest> T setHeaders(@Nonnull final T httpMethod) {
		httpMethod.setHeader(CONTENT_TYPE, CONTENT_TYPE_JSON);

		return httpMethod;
	}

}
