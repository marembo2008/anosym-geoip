package anosym.geoip.provider.nekudo;

import javax.annotation.Nullable;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import anosym.common.Country;
import anosym.geoip.provider.jsonutil.CountryCodeDserializer;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Oct 4, 2015, 10:51:42 AM
 */
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
class CountryInfo {

	@Nullable
	private String name;

	@Nullable
	@JsonProperty("code")
	@JsonDeserialize(using = CountryCodeDserializer.class)
	private Country country;
}
