package anosym.geoip.provider.nekudo;

import java.math.BigDecimal;
import java.util.TimeZone;

import javax.annotation.Nullable;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import anosym.geoip.provider.jsonutil.TimeZoneDeserializer;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Oct 4, 2015, 10:52:54 AM
 */
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
class LocationInfo {

	@Nullable
	private BigDecimal latitude;

	@Nullable
	private BigDecimal longitude;

	@Nullable
	@JsonDeserialize(using = TimeZoneDeserializer.class)
	private TimeZone timeZone;
}
