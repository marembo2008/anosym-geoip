package anosym.geoip.provider.nekudo;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Strings.nullToEmpty;

import javax.annotation.Nonnull;

import anosym.common.Country;
import anosym.geoip.GeoIp;
import anosym.geoip.provider.GeoIpProvider;
import anosym.geoip.provider.ProviderConfigurationService;
import anosym.geoip.provider.ProviderScope;
import anosym.geoip.provider.RestServiceProvider;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Oct 4, 2015, 10:49:05 AM
 */
@ApplicationScoped
@ProviderScope(id = "http://geoip.nekudo.com", priority = -1)
public class NekudoGeoIpProvider extends RestServiceProvider implements GeoIpProvider {

	@Inject
	private NekudoGeoIpConfigurationService nekudoGeoIpConfigurationService;

	@Nonnull
	@Override
	protected ProviderConfigurationService providerConfigurationService() {
		return nekudoGeoIpConfigurationService;
	}

	@Override
	public GeoIp provide(@Nonnull final String ip) {
		checkNotNull(ip, "The ip must not be null");

		final NekudoGeoIpInfo geoIpInfo = executeGet(ip, NekudoGeoIpInfo.class);
		if (geoIpInfo == null) {
			return null;
		}

		final CountryInfo countryInfo = geoIpInfo.getCountryInfo();
		if (countryInfo == null) {
			return null;
		}

		final Country country = countryInfo.getCountry();
		if (country == null) {
			return null;
		}

		return GeoIp.builder().city(nullToEmpty(geoIpInfo.getCity())).country(country)
				.countryName(nullToEmpty(countryInfo.getName())).ip(geoIpInfo.getIp()).build();
	}

}
