package anosym.geoip.provider.nekudo;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Oct 4, 2015, 10:50:14 AM
 */
@Getter
@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor(access = AccessLevel.PROTECTED)
class NekudoGeoIpInfo {

	@Nullable
	private String city;

	@Nullable
	@JsonProperty("country")
	private CountryInfo countryInfo;

	@Nullable
	@JsonProperty("location")
	private LocationInfo locationInfo;

	@Nonnull
	private String ip;
}
