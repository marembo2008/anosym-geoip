package anosym.geoip.provider.nekudo;

import javax.annotation.Nonnull;

import anosym.geoip.provider.ProviderConfigurationService;
import khameleon.core.annotations.Config;
import khameleon.core.annotations.ConfigRoot;
import khameleon.core.annotations.Default;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Oct 4, 2015, 11:02:03 AM
 */
@Config
@ConfigRoot
public interface NekudoGeoIpConfigurationService extends ProviderConfigurationService {

	@Nonnull
	@Override
	@Default("http://geoip.nekudo.com/api/")
	String getEndPointUrl();

}
