package anosym.geoip.persistence;

import java.io.Serializable;

import javax.annotation.Nonnull;

import anosym.geoip.GeoIp;

/**
 *
 * @author marembo
 */
public interface GeoIpPersistenceService extends Serializable {

    void persist(@Nonnull final GeoIp geoIp);

}
