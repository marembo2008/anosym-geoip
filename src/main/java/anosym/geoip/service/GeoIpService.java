package anosym.geoip.service;

import java.io.Serializable;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import anosym.geoip.GeoIp;

/**
 *
 * @author marembo
 */
public interface GeoIpService extends Serializable {

	@Nullable
	GeoIp findIp(@Nonnull final String ip);
}
