package anosym.geoip.service.impl;

import static com.google.common.base.Preconditions.checkNotNull;
import static java.lang.String.format;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;

import anosym.cache.Cacheable;
import anosym.geoip.GeoIp;
import anosym.geoip.persistence.GeoIpPersistenceService;
import anosym.geoip.provider.GeoIpProvider;
import anosym.geoip.provider.ProviderScope;
import anosym.geoip.service.GeoIpConfigurationService;
import anosym.geoip.service.GeoIpService;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.enterprise.inject.Any;
import jakarta.enterprise.inject.Default;
import jakarta.enterprise.inject.Instance;
import jakarta.inject.Inject;

/**
 *
 * @author marembo
 */
@Default
@ApplicationScoped
public class DefaultGeoIpService implements GeoIpService {

	private static final Logger LOG = Logger.getLogger(DefaultGeoIpService.class.getName());

	private static final Comparator<GeoIpProvider> GEOIP_COMPARATOR = (provider1, provider2) -> {
		final ProviderScope providerScope1 = provider1.getClass().getAnnotation(ProviderScope.class);
		final ProviderScope providerScope2 = provider2.getClass().getAnnotation(ProviderScope.class);
		return Integer.valueOf(providerScope1.priority()).compareTo(providerScope2.priority());
	};

	private final Iterable<GeoIpProvider> providers;

	private final Instance<GeoIpPersistenceService> persistenceService;

	private final Instance<GeoIpConfigurationService> geoIpConfigurationService;

	@Inject
	public DefaultGeoIpService(@Any final Instance<GeoIpConfigurationService> geoIpConfigurationService,
			@Any final Instance<GeoIpProvider> providers,
			@Any final Instance<GeoIpPersistenceService> persistenceService) {
		this.geoIpConfigurationService = geoIpConfigurationService;
		this.providers = sort(providers);
		this.persistenceService = persistenceService;
	}

	private String providerId(@Nonnull final GeoIpProvider provider) {
		return provider.getClass().getAnnotation(ProviderScope.class).id();
	}

	private boolean isProviderEnabled(@Nullable final GeoIpProvider input) {
		if (!geoIpConfigurationService.isAmbiguous() && !geoIpConfigurationService.isUnsatisfied()) {
			final GeoIpConfigurationService configurationService = geoIpConfigurationService.get();
			final String id = input == null ? "no-provider-id" : providerId(input);
			final boolean isEnabled = configurationService.isGeoIpProviderEnabled(id);
			LOG.log(Level.INFO, "GeoIpProvider Enabled({0}={1})", new Object[] { id, isEnabled });

			return isEnabled;
		}
		return true;
	}

	@Nonnull
	private Iterable<GeoIpProvider> sort(final Iterable<GeoIpProvider> providers) {
		final List<GeoIpProvider> listProviders = Lists.newArrayList(providers);
		Collections.sort(listProviders, GEOIP_COMPARATOR);
		LOG.log(Level.INFO, "Registered GeoIP providers: {0}", listProviders);

		return ImmutableList.copyOf(listProviders);
	}

	@Override
	@Nullable
	@Cacheable(providerId = "inMemoryCacheProvider", lifeTime = 30l, unit = TimeUnit.DAYS)
	public GeoIp findIp(@Nonnull final String ip) {
		checkNotNull(ip, "the ip to find geoip data must be specified");

		final GeoIp geoip = ImmutableList.copyOf(providers).stream().filter(this::isProviderEnabled)
				.map((provider) -> load(provider, ip)).filter(Objects::nonNull).findFirst().orElse(null);
		if (geoip == null) {
			return null;
		}

		if (!persistenceService.isAmbiguous() && !persistenceService.isUnsatisfied()) {
			persistenceService.get().persist(geoip);
		}

		return geoip;
	}

	@Nullable
	private GeoIp load(@Nonnull final GeoIpProvider provider, @Nonnull final String ip) {
		try {
			return provider.provide(ip);

		} catch (final Exception ex) {
			LOG.log(Level.WARNING,
					format("GeoIp Lookup failed for provider: {%s=%s}", provider.getId(), ex.getLocalizedMessage()));
		}
		return null;
	}

	private int lookupTimeout() {
		if (geoIpConfigurationService.isUnsatisfied()) {
			return 200;
		}

		return geoIpConfigurationService.get().getLookupTimeout();
	}

}
