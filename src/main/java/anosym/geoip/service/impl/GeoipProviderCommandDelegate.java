package anosym.geoip.service.impl;

import static com.netflix.hystrix.HystrixCommandGroupKey.Factory.asKey;
import static java.util.Objects.requireNonNull;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.netflix.hystrix.HystrixCommand;

import anosym.geoip.GeoIp;
import anosym.geoip.provider.GeoIpProvider;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Oct 3, 2015, 11:17:17 AM
 */
public class GeoipProviderCommandDelegate extends HystrixCommand<GeoIp> {

	private final GeoIpProvider geoIpProvider;

	private final String lookupIp;

	public GeoipProviderCommandDelegate(@Nonnull final GeoIpProvider geoIpProvider, @Nonnull final String lookupIp,
			final int lookUpTimeout) {
		super(asKey("GeoIpService"), lookUpTimeout);
		this.geoIpProvider = requireNonNull(geoIpProvider, "The geoIpProvider must not be null");
		this.lookupIp = requireNonNull(lookupIp, "The lookupIp must not be null");

	}

	@Nullable
	@Override
	protected GeoIp run() throws Exception {
		return geoIpProvider.provide(lookupIp);
	}

	@Nullable
	@Override
	protected GeoIp getFallback() {
		return null;
	}

}
