package anosym.geoip.service;

import java.io.Serializable;

import khameleon.core.annotations.Config;
import khameleon.core.annotations.ConfigDelete;
import khameleon.core.annotations.ConfigParam;
import khameleon.core.annotations.ConfigRoot;
import khameleon.core.annotations.Default;
import khameleon.core.annotations.Info;

/**
 *
 * @author marembo
 */
@Config
@ConfigRoot
public interface GeoIpConfigurationService extends Serializable {

	@Default("true")
	@Info("Whether the geoip provider with the specified name is enabled or not. By default all providers are enabled.")
	boolean isGeoIpProviderEnabled(@ConfigParam(name = "providerId") final String providerId);

	@Default("100")
	@ConfigDelete
	@Info("The geoip lookup timeout. if we fail to find the geoip information, we continue without country information")
	int getLookupTimeout();
}
