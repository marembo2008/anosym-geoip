package anosym.geoip;

import java.io.Serializable;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import anosym.common.Country;
import anosym.geoip.provider.jsonutil.CountryCodeDserializer;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 *
 * @author marembo
 */
@Getter
@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class GeoIp implements Serializable {

	private static final long serialVersionUID = 343843843841L;

	@Nonnull
	@JsonProperty("ip")
	private String ip;

	@Nullable
	@JsonProperty("country_code")
	@JsonDeserialize(using = CountryCodeDserializer.class)
	private Country country;

	@Nullable
	@JsonProperty("country_name")
	private String countryName;

	@Nullable
	@JsonProperty("region_code")
	private String regionCode;

	@Nullable
	@JsonProperty("region_name")
	private String regionName;

	@Nullable
	@JsonProperty("city")
	private String city;

	@Nullable
	@JsonProperty("zip_code")
	private String zipCode;

	@Nullable
	@JsonProperty("time_zone")
	private String timeZone;

	@JsonProperty("latitude")
	private double latitude;

	@JsonProperty("longitude")
	private double longitude;

	@JsonProperty("metro_code")
	private int metroCode;

}
